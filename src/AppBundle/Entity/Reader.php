<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reader
 *
 * @ORM\Table(name="reader")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReaderRepository")
 */
class Reader
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=127)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="adress", type="string")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="number_id", type="string")
     */
    private $numberId;

    /**
     * @var string
     *
     * @ORM\Column(name="libraryCard", type="string")
     */
    private  $libraryCard;

    /**
     * @ORM\OneToMany(targetEntity="Application", mappedBy="reader")
     */
    private $applications;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Reader
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set adress
     *
     * @param string $address
     *
     * @return Reader
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get adress
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set numberId
     *
     * @param string $numberId
     *
     * @return Reader
     */
    public function setNumberId($numberId)
    {
        $this->numberId = $numberId;

        return $this;
    }

    /**
     * Get numberId
     *
     * @return string
     */
    public function getNumberId()
    {
        return $this->numberId;
    }

    /**
     * @param string $libraryCard
     * @return Reader
     */
    public function setLibraryCard($libraryCard)
    {
        $this->libraryCard = $libraryCard;
        return $this;
    }

    /**
     * @return string
     */
    public function getLibraryCard()
    {
        return $this->libraryCard;
    }

    /**
     * @param mixed $applications
     * @return Reader
     */
    public function setApplications($applications)
    {
        $this->applications = $applications;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getApplications()
    {
        return $this->applications;
    }
}

