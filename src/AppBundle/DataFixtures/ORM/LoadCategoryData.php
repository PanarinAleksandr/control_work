<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategoryData extends Fixture implements DependentFixtureInterface
{


    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $book1 =$this->getReference(LoadBookData::BOOK_ONE);
        $book2 =$this->getReference(LoadBookData::BOOK_TWO);
        $book3 =$this->getReference(LoadBookData::BOOK_THREE);
        $book4 =$this->getReference(LoadBookData::BOOK_FOUR);
        $book5 =$this->getReference(LoadBookData::BOOK_FIVE);
        $book6 =$this->getReference(LoadBookData::BOOK_SIX);
        $book7 =$this->getReference(LoadBookData::BOOK_SEVEN);
        $book8 =$this->getReference(LoadBookData::BOOK_EIGHT);
        $book9 =$this->getReference(LoadBookData::BOOK_NINE);
        $book10 =$this->getReference(LoadBookData::BOOK_TEN);

        $books =[
            $book1,
            $book2,
            $book3,
            $book4,
            $book5,
            $book6,
            $book7,
            $book8,
            $book9,
            $book10
        ];
        $ganre = ['Ужасы','Остросюжетные','Детские','религия',
            'проза','Ужасы','Остросюжетные','Детские','религия',
            'Остросюжетные'];
        for($i = 0; $i < count($ganre); $i++) {
            $book = new Category();
            $book->setName($ganre.$i)
                ->setBooks($books[$i]);
            $manager->persist($book);
        }
        $manager->flush();

    }


    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return [
            LoadBookData::class
        ];
    }
}