<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBookData extends Fixture
{

    const BOOK_ONE = 'Прощай, оружие';
    const BOOK_TWO = 'Повелитель мух';
    const BOOK_THREE = 'Чапаев и Пустота';
    const BOOK_FOUR = 'Бойцовский клуб';
    const BOOK_FIVE ='451 градус по Фаренгейту';
    const BOOK_SIX ='Убить пересмешника';
    const BOOK_SEVEN ='Мартин Иден';
    const BOOK_EIGHT ='Учение Дона Хуана';
    const BOOK_NINE ='Форрест Гамп';
    const BOOK_TEN ='Аэропорт';
        /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $boks = [ self::BOOK_ONE =>'5b0bf5c416668.jpg', self::BOOK_TWO =>'5b0bf64a59a17.jpg',
            self::BOOK_THREE =>'5b0c35b0e47bd.jpg',
            self::BOOK_FOUR => '5b0c36ffe6f44.jpg',
            self::BOOK_FIVE=>'5b0bf5c416668.jpg',
            self::BOOK_SIX =>'5b0bf64a59a17.jpg',
            self::BOOK_EIGHT =>'5b0c35b0e47bd.jpg',
            self::BOOK_NINE =>'5b0c36ffe6f44.jpg',
            self::BOOK_TEN =>'5b0bf5c416668.jpg'];


    $book1 = new Book();
    $book1->setName(self::BOOK_ONE)
        ->setImage('5b0bf5c416668.jpg')
        ->setAuthor('Какой то ватор');
        $manager->persist($book1);

        $book2 = new Book();
        $book2->setName(self::BOOK_TWO)
            ->setImage('5b0bf5c416668.jpg')
            ->setAuthor('Какой то ватор');
        $manager->persist($book2);

        $book3 = new Book();
        $book3->setName(self::BOOK_THREE)
            ->setImage('5b0bf5c416668.jpg')
            ->setAuthor('Какой то ватор');
        $manager->persist($book3);

        $book4 = new Book();
        $book4->setName(self::BOOK_FOUR)
            ->setImage('5b0bf5c416668.jpg')
            ->setAuthor('Какой то ватор');
        $manager->persist($book4);

        $book5 = new Book();
        $book5->setName(self::BOOK_FIVE)
            ->setImage('5b0bf5c416668.jpg')
            ->setAuthor('Какой то ватор');
        $manager->persist($book5);

        $book6 = new Book();
        $book6->setName(self::BOOK_SIX)
            ->setImage('5b0bf5c416668.jpg')
            ->setAuthor('Какой то ватор');
        $manager->persist($book6);

        $book7 = new Book();
        $book7->setName(self::BOOK_SEVEN)
            ->setImage('5b0bf5c416668.jpg')
            ->setAuthor('Какой то ватор');
        $manager->persist($book7);

        $book8 = new Book();
        $book8->setName(self::BOOK_EIGHT)
            ->setImage('5b0bf5c416668.jpg')
            ->setAuthor('Какой то ватор');
        $manager->persist($book8);

        $book9 = new Book();
        $book9->setName(self::BOOK_NINE)
            ->setImage('5b0bf5c416668.jpg')
            ->setAuthor('Какой то ватор');
        $manager->persist($book9);

        $book10 = new Book();
        $book10->setName(self::BOOK_TEN)
            ->setImage('5b0bf5c416668.jpg')
            ->setAuthor('Какой то ватор');
    $manager->persist($book10);

    $this->addReference(self::BOOK_ONE, $book1);
    $this->addReference(self::BOOK_TWO, $book2);
    $this->addReference(self::BOOK_THREE, $book3);
    $this->addReference(self::BOOK_FOUR, $book4);
    $this->addReference(self::BOOK_FIVE, $book5);
    $this->addReference(self::BOOK_SIX, $book6);
    $this->addReference(self::BOOK_SEVEN, $book7);
    $this->addReference(self::BOOK_EIGHT, $book8);
    $this->addReference(self::BOOK_NINE, $book9);
    $this->addReference(self::BOOK_TEN, $book10);

        $manager->flush();

    }


}