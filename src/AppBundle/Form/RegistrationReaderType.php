<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationReaderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',TextType::class , ['label'=> 'Введите ваше имя' ]);
        $builder->add('address',TextType::class , ['label'=> 'Адрес' ]);
        $builder->add('numberId',TextType::class , ['label'=> 'Введите номер паспорта' ]);
        $builder->add('save',SubmitType::class, ['label'=> 'Получить читательский билет']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_registration_reader_type';
    }
}
