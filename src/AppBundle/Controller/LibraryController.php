<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class LibraryController extends Controller
{
    /**
     * @Route("/liLibrary", name="library")
     */
    public function indexAction()
    {

        return $this->render('@App\Library\new.html.twig', array(

        ));
    }

    /**
     * @Route("/create")
     */
    public function createAction()
    {
        return $this->render('AppBundle:Library:create.html.twig', array(
            // ...
        ));
    }

}
