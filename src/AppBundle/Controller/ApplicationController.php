<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Application;
use AppBundle\Form\ApplicationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class ApplicationController extends Controller
{
    /**
     * @Route("/index")
     */
    public function newAction()
    {

        return $this->render('@App\Application\new.html.twig', array(

        ));
    }

    /**
     * @Route("/create",name="application_create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {

        $application = new Application();

        $form = $this->createForm(ApplicationType::class,$application);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $array = $request->request->all();

            foreach ($array  as $key => $val ){
                $readerIC = $val['reader'];
            }
            $result = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Reader')
                ->createQueryBuilder('r')
                ->select('r')
                ->where("r.libraryCard = '{$readerIC}'")
                ->getQuery()
                ->getResult();

            foreach ($result as $key => $id){
                $readerId = $id;
            }

            $em = $this->getDoctrine()->getManager();

            $application->setReader($readerId);

            $application->setStatus('Ожидается');
            $em->persist($application);
            $em->flush();

            return $this->redirectToRoute('homepage');

        }
        return $this->render('@App\Application\create.html.twig', array(
            'form' => $form->createView()
        ));
    }

}
