<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Reader;
use AppBundle\Form\RegistrationReaderType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;

class ReaderController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {

        $books = $this->getDoctrine()
            ->getRepository('AppBundle:Book')
            ->findAll();

        return $this->render('@App\Reader\index.html.twig', array(
            'books' =>$books
        ));
    }

    /**
     * @Route("/registration" , name="registration")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $reader = new Reader();
        $form = $this->createForm(RegistrationReaderType::class, $reader);
        $form->handleRequest($request);
        $rand = rand(1,1000);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $reader->setLibraryCard($reader->getNumberId()."{$rand}");
            $em->persist($reader);
            $em->flush();

            return $this->redirectToRoute('greeting');
        }

        return $this->render('@App\Reader\registration.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/greeting" , name="greeting")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function greetingAction(Request $request)
    {


        return  $this->render('@App/Reader/greeting.html.twig',[
            'data'=> $request
            ]

        );
    }

}
